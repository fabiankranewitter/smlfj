package com.fabiankranewitter.smlfj.signature;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.CodeSigner;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Logger;

public final class JarSignature {

	public static final String TAG = JarSignature.class.getSimpleName();
	private static Logger LOGGER = Logger.getLogger(TAG);

	private boolean jarSigned = false;
	private boolean jarVerified = false;

	public static JarSignature startJarVerification(URL jarUrl) throws IOException {
		return new JarSignature(jarUrl);
	}

	private JarSignature(URL jarUrl) throws IOException {
		verification(jarUrl);
	}

	private void verification(URL jarUrl) throws IOException {
		JarFile jarFile = new JarFile(jarUrl.getFile(), true);
		Enumeration<JarEntry> jarEntrys = jarFile.entries();
		boolean verified = true;
		while (jarEntrys.hasMoreElements()) {
			JarEntry jarEntry = jarEntrys.nextElement();
			try {
				InputStream inputStream = jarFile.getInputStream(jarEntry);
				inputStream.readAllBytes(); // the complete file must be read because otherwise the verification will
											// not work

				CodeSigner[] codeSigners = jarEntry.getCodeSigners();
				boolean entrySigned = (codeSigners != null && codeSigners.length != 0);
				this.jarSigned |= entrySigned; // when a file is signed, the entire jar is signed

			} catch (SecurityException e) {
				LOGGER.warning(
						"file '" + jarEntry.getName() + "' in jar '" + jarFile.getName() + "' could not be verified");
				verified = false;
				break;
			}
		}

		this.jarVerified = verified;

		if (!jarSigned) { // if jar not signed then the jar cannot be verified either
			this.jarVerified = false;
		}

		jarFile.close();
	}

	public boolean isJarSigned() {
		return jarSigned;
	}

	public boolean isJarVerified() {
		return jarVerified;
	}

}
