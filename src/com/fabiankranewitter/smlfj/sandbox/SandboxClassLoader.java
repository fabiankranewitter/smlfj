package com.fabiankranewitter.smlfj.sandbox;

import java.net.URL;
import java.net.URLClassLoader;
import java.security.CodeSource;
import java.security.PermissionCollection;
import java.security.Permissions;

public class SandboxClassLoader extends URLClassLoader {

	public static final String TAG = SandboxClassLoader.class.getSimpleName();

	static {
		ClassLoader.registerAsParallelCapable();
	}

	private final Permissions permissions;

	public SandboxClassLoader(URL jarFileUrl, Permissions permissions) {
		super(new URL[] { jarFileUrl });
		this.permissions = permissions;
	}

	@Override
	protected PermissionCollection getPermissions(CodeSource codesource) {
		return this.permissions;
	}

}
