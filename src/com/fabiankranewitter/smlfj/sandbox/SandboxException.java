package com.fabiankranewitter.smlfj.sandbox;

public class SandboxException extends Exception {

	private static final long serialVersionUID = 709050672506468156L;

	public SandboxException(String s) {
		super(s);
	}

	public SandboxException() {
		super();
	}

}
