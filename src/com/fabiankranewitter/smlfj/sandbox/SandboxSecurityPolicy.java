package com.fabiankranewitter.smlfj.sandbox;

import java.security.AllPermission;
import java.security.PermissionCollection;
import java.security.Permissions;
import java.security.Policy;
import java.security.ProtectionDomain;

public final class SandboxSecurityPolicy extends Policy {

	public static final String TAG = SandboxSecurityPolicy.class.getSimpleName();

	@Override
	public PermissionCollection getPermissions(ProtectionDomain domain) {
		if (isModul(domain)) {
			return getModulPermissions();
		} else {
			return getCorePermissions();
		}
	}

	private boolean isModul(ProtectionDomain domain) {
		return domain.getClassLoader() instanceof SandboxClassLoader;
	}

	private PermissionCollection getModulPermissions() {
		return new Permissions(); // no permissions
	}

	private PermissionCollection getCorePermissions() {
		Permissions permissions = new Permissions();
		permissions.add(new AllPermission());
		return permissions;
	}
}