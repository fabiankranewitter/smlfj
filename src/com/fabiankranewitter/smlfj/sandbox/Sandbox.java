package com.fabiankranewitter.smlfj.sandbox;

import java.security.Policy;
import java.util.logging.Logger;

public final class Sandbox {

	public static final String TAG = Sandbox.class.getSimpleName();
	private static Logger LOGGER = Logger.getLogger(TAG);

	private Sandbox() {
	}

	public static boolean enable() throws SandboxException, SecurityException {
		LOGGER.info("try to enable sandbox");
		Policy.setPolicy(new SandboxSecurityPolicy());
		System.setSecurityManager(new SecurityManager());
		if (isEnabled()) {
			LOGGER.info("sandbox enabled");
			return true;
		} else {
			LOGGER.severe("sandbox is not enabled");
			throw new SandboxException("initialization of sandbox failed");
		}
	}

	public static boolean isEnabled() throws SecurityException {
		return System.getSecurityManager() != null && Policy.getPolicy() instanceof SandboxSecurityPolicy;
	}

}
