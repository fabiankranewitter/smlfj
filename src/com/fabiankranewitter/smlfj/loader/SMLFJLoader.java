package com.fabiankranewitter.smlfj.loader;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.security.Permissions;

import com.fabiankranewitter.smlfj.sandbox.SandboxClassLoader;

public final class SMLFJLoader {

	public static final String TAG = SMLFJLoader.class.getSimpleName();

	private SMLFJLoader() {}

	public static Object load(URL jarUrl, String classname, Permissions permissions)
			throws ClassNotFoundException, InstantiationException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, NoSuchMethodException, SecurityException, IOException {
		SandboxClassLoader classLoader = new SandboxClassLoader(jarUrl, permissions);
		Object instance = load(classLoader, classname);
		classLoader.close();
		return instance;
	}

	public static Object load(SandboxClassLoader classLoader, String classname)
			throws ClassNotFoundException, InstantiationException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, NoSuchMethodException, SecurityException {
		Class<?> clazz = classLoader.loadClass(classname);
		return clazz.getConstructor().newInstance();
	}

	@SuppressWarnings("unchecked")
	public static <T> T load(URL jarPath, String classname, Permissions permissions, Class<T> clazz)
			throws ClassNotFoundException, InstantiationException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, NoSuchMethodException, SecurityException, IOException {
		return (T) load(jarPath, classname, permissions);
	}

	@SuppressWarnings("unchecked")
	public static <T> T load(SandboxClassLoader classLoader, String classname, Class<T> clazz)
			throws ClassNotFoundException, InstantiationException, IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, NoSuchMethodException, SecurityException {
		return (T) load(classLoader, classname);
	}

}
