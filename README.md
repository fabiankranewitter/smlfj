# Secure module loader for Java (smlfj)

## Warning
>  Deprecated since version JDK 17 ([JEP 411](https://openjdk.org/jeps/411))  

## Description
This code loads external jars with the SandboxClassLoader. This activates the security policy of the Java VM. Each jar can have its own permission. The program that activated the sandbox still retains all of its permission. There are a lot of different permissions like for file operations, sockets .... Here is a [link](https://cr.openjdk.java.net/~iris/se/11/latestSpec/api/java.base/java/security/Permission.html) of the permission. But it is also possible to write your own permissions. If a jar violates the permission, it throws a [SecurityException](https://docs.oracle.com/javase/7/docs/api/java/lang/SecurityException.html).

## Status
### Works already
* [x] Sandbox
* [x] Permission system
* [x] Load external jars files
* [x] Jar verification

### In progress

### Planned
* [ ] Jar signature verification

## Signature
In order to verify the jar and to verify the signature later, the jar must be signed. If this is not used, the parameter `verifyJarSignature` must be set to false. Otherwise true. The review checks whether the jar has been tampered with. But not whether it was exchanged for another signed jar. (Planned)
### Generate Keys
For example, a keystore was created here with PKCS12 and RSA and a keysize of 4096. The certificate is valid for 10 years.  
`keytool -genkey -alias <alias-name> -storetype PKCS12 -keystore <keystore-name.p12> -keyalg RSA -keysize 4096 -validity 3650`
### Jar signing
Then you sign the jar with the supplied (JDK) jarsigner.  
`jarsigner -keystore <keystore-name.p12> -signedjar <output.jar> <input.jar> <alias-name>`

## Example

```
try {
	// the sandbox must be activated at startup
	Sandbox.enable();
} catch (SecurityException | SandboxException e) {
	// if the sandbox cannot be activated, e.g. due to another restriction, an error will be thrown here
	e.printStackTrace();
}

// add the permission here that the jar may use
Permissions permissions = new Permissions();
//permissions.add(...);
		
// url to the jar to be loaded
URL jarURL = jarURL = Paths.get("demo.jar").toUri().toURL();
		
//to check the jar signature call startJarVerification. this can take some time depending on the size of the jar.
JarSignature jarSignature = JarSignature.startJarVerification(jarURL);
		
//isjarsidned provides information on whether this jar has been signed or not
if(!jarSignature.isJarSigned()) {
	System.err.println("jar is not signed");
	return;
}
		
//isJarVerified indicates whether the signature hashes match the files
if(!jarSignature.isJarVerified()) {
	System.err.println("jar signature could not be verified");
	return;
}
		
		
// the class of which an instance is to be created
// the standard constructor is used for this
String className = "com.example.demo.Demo";
		
try {
	// creates an instance from the jar file, the instance will have the same type as the last parameter
	Object obj = SMLFJLoader.load(jarURL, className, permissions, Object.class); 
	
	// ...
		    
} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | IllegalArgumentException
| InvocationTargetException | NoSuchMethodException | SecurityException | IOException e) {
	// other errors such as file or class do not exist
	e.printStackTrace();
}
```
